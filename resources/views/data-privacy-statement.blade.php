@extends('layouts.app-guess')

@section('content')
<div class="row">
  <div class="col-md-12">
  <div class=WordSection1>

  <p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:
  normal'><span style='font-family:"Times New Roman","serif"'>Data Privacy
  Statement<o:p></o:p></span></b></p>

  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-family:"Times New Roman","serif"'>GENERAL STATEMENT<o:p></o:p></span></b></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'>This
  Data Privacy Statement, which has been prepared in relation to R.A. 10173,
  otherwise known as Data Privacy Act of 2012, and it’s implementing rules and
  regulations, describes how personal information are collected, processed,
  disclosed, and stored by International Computer Technology College (“ICT
  College”) and is applicable to all persons who apply for or avail of any ICT
  College’s services, or who has established or propose to establish an account
  with ICT College, or who have provided or propose to provide third party
  security to ICT College (“Client”).<o:p></o:p></span></p>

  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-family:"Times New Roman","serif"'>WHAT WE GET?<o:p></o:p></span></b></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'>To
  provide the client with ICT College’s enrollment for educational services, ICT
  College may collect personal information from the client which may include, but
  are not limited to:<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;mso-list:l1 level1 lfo1'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  color:red'><span style='mso-list:Ignore'>(a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif";
  color:red'>Name<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;mso-list:l1 level1 lfo1'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  color:red'><span style='mso-list:Ignore'>(b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif";
  color:red'><span style='mso-spacerun:yes'> </span>Place/Date of Birth<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;mso-list:l1 level1 lfo1'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  color:red'><span style='mso-list:Ignore'>(c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif";
  color:red'><span style='mso-spacerun:yes'> </span>Gender<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;mso-list:l1 level1 lfo1'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  color:red'><span style='mso-list:Ignore'>(d)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif";
  color:red'><span style='mso-spacerun:yes'> </span>Citizenship <o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in;mso-list:l1 level1 lfo1'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  color:red'><span style='mso-list:Ignore'>(e)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif";
  color:red'>Address and Contact Details<o:p></o:p></span></p>

  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-family:"Times New Roman","serif"'>WHAT WE WILL USE THEM FOR<o:p></o:p></span></b></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'>ICT
  College may request client’s personal information in connection with any of the
  following purposes:<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;mso-list:l0 level1 lfo2'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  style='mso-list:Ignore'>(a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif"'>To
  view and maintain accounts;<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;mso-list:l0 level1 lfo2'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  style='mso-list:Ignore'>(b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif"'>Ease
  of contacting / communicating with clients;<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;mso-list:l0 level1 lfo2'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  style='mso-list:Ignore'>(c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif"'>Sending
  of student number;<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in;mso-list:l0 level1 lfo2'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  style='mso-list:Ignore'>(d)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif"'>Maintain
  know-your-customer information;<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in;mso-list:l0 level1 lfo2'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  style='mso-list:Ignore'>(e)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif"'>To
  evaluate, approve, provide, or manage financial products and services, and
  other transactions that the client has requested;<o:p></o:p></span></p>

  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-family:"Times New Roman","serif"'>METHOD OF PROCESSING PERSONAL
  DATA<o:p></o:p></span></b></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'>Processing
  refers to the collection, recording, organization, storage, updating or
  modification, retrieval, use, blocking of personal information.<o:p></o:p></span></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'>ICT
  College may collect personal information through, but not limited to, any of
  the following:<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in;mso-list:l2 level1 lfo3'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  style='mso-list:Ignore'>(a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif"'>Accomplishment
  and/or signing of forms/documents<o:p></o:p></span></p>

  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in;mso-list:l2 level1 lfo3'><![if !supportLists]><span
  style='font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  style='mso-list:Ignore'>(b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
  </span></span></span><![endif]><span style='font-family:"Times New Roman","serif"'>Online
  Enrollment through online enrollment system (Online Enrollment for ICT College)<o:p></o:p></span></p>

  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-family:"Times New Roman","serif"'>WHEN DO WE COLLECT<o:p></o:p></span></b></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'>ICT
  College collects information upon the client’s application for <span
  class=SpellE>availment</span> of and/or usage of ICT College’s products and
  services. This includes to account opening/ signing in to Online Enrollment for
  ICT College.<o:p></o:p></span></p>

  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></b></p>

  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-family:"Times New Roman","serif"'>RECIPIENTS OF INFORMATION<o:p></o:p></span></b></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'>Personal
  information may be processed within the whole administration of the ICT College
  to any of the purposes herein declared, to save registration and to process
  payment transactions.<o:p></o:p></span></p>

  <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
  style='font-family:"Times New Roman","serif"'>DATA PROTECTION OFFICER<o:p></o:p></span></b></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'>Any
  inquiry or request for information regarding this statement may be addressed to
  the following:<o:p></o:p></span></p>

  <p class=MsoNormal style='text-indent:.5in'><b style='mso-bidi-font-weight:
  normal'><span style='font-family:"Times New Roman","serif"'>Data Protection
  Officer<o:p></o:p></span></b></p>

  <p class=MsoNormal style='text-indent:.5in'><span style='font-family:"Times New Roman","serif"'>ICT
  College<o:p></o:p></span></p>

  <p class=MsoNormal style='text-indent:.5in'><span style='font-family:"Times New Roman","serif"'>Tarlac,
  City<o:p></o:p></span></p>

  <p class=MsoNormal><span style='font-family:"Times New Roman","serif"'><o:p>&nbsp;</o:p></span></p>

  </div>  
  </div>
</div>
@endsection