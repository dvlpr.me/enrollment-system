@extends('layouts.app')

@section('title') Faculty @endsection

@section('headside')
    @include('faculty.includes.header')
    @include('faculty.includes.side-menu')
@endsection


@section('content')
<p><strong>Faculty Dashboard</strong></p>
@include('includes.all')
@endsection